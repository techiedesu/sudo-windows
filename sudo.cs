using System;
using System.Diagnostics;
using System.Linq;

namespace sudo
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                if (args.Length == 0) return;
                string argsStr = "";
                if (args.Length > 1) { argsStr = String.Join("\"\" ", 
                    args.Skip(1).ToArray().Select(p => p.ToString()).ToArray()); }

                var start = new ProcessStartInfo()
                {
                    Arguments = argsStr,
                    FileName = args[0]
                };

                new Process { StartInfo = start }.Start();
            }
            catch { }
        }
    }
}
